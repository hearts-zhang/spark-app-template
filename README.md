## Spark's Gradle Project DEMO

### spark-default.conf

```conf
spark.master                      yarn
spark.eventLog.dir                hdfs://hadoop@namenode:8020/dw/nara/spark-logs
spark.ui.enabled                  true
spark.ui.killEnabled              true
spark.history.fs.logDirectory     hdfs://hadoop@namenode:8020/dw/nara/history-logs
spark.eventLog.enabled            true
spark.executor.memory             8g
spark.executor.cores              4
spark.executor.instances          8
```

- update $SPARK_HOME/conf/spark-default.conf
- dynamic-allocation is not supported by yarn

### log4j.properties

```conf
log4j.rootCategory=WARN, console
log4j.appender.console.layout.ConversionPattern=%d{HH:mm:ss} %p %c{1}: %m%n
```

- $SPARK_HOME/conf/log4j.properties

### spark-env.sh

```sh
export HADOOP_CONF_DIR=/usr/local/hadoop/etc/hadoop
export HADOOP_USER_NAME=hadoop
export LD_LIBRARY_PATH=${HADOOP_HOME}/lib/native/:$LD_LIBRARY_PATH
# supress warning no hadoop native libs found...
```

### /etc/hosts

```conf
## namenode && resourcemanager
10.1.7.128 funshion-newhadoop128 namenode-128 namenode
10.1.7.129 funshion-newhadoop129 namenode-129

...
```

- make sure namenodes are named *namenode*


### Attentions

- gradle --version > 4 is required
- export HADOOP_USER_NAME=hadoop if current user isn't hadoop

### usage

```sh
gradle shadow
spark-submit build/libs/spark-app-template-0.1.0-all.jar
```

- jar已经指定 Main-Class,  不用在命令行再次指定
- 其他环境参数在spark-default.conf中已经指定
- *-all.jar 是一个fat jar，使用shadowJar制作
