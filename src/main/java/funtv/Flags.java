package funtv;

import com.beust.jcommander.Parameter;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.nio.file.Path;
public class Flags {
  @Parameter(names = "-debug", description = "Debug mode")
  protected boolean debug = false;

  @Parameter(names = "-name", description = "")
  protected String name = "spark-gradle-demo";

  @Parameter(names = "-prefix", description = "")
  protected String prefix = "/dw/nara";

  @Parameter(names = "-hdfs", description = "")
  protected String hdfs = "hdfs://hadoop@10.1.7.128:8020/dw/nara";

  @Parameter(names = "-file", description = "")
  protected String file = "demo/users.id";

  @Parameter(names = "-word", description = "")
  protected String word = "b43c82f8caefcbefc8efc8ef80136afb99ea406b7cf1c1cc7cd3ac23e69ede42";

  protected String input(){
    String input = Paths.get(prefix).resolve(file).toString();
    try{
      Path pth = Paths.get(prefix);
      pth = pth.resolve(file);

      URL uri = new URL(hdfs);
      uri = new URL(uri, pth.toString());
      input = uri.toString();
    }catch(MalformedURLException ex){}
    return input;
  }
}
