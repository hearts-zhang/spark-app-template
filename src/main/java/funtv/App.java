package funtv;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.beust.jcommander.JCommander;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.JavaSparkContext;

public class App {
  private static final Flags flags = new Flags();
  //private static final Logger logger = LoggerFactory.getLogger(App.class);

  public static void main(String[] args) {
    // TODO: why noBuilder throws nosuchmethoderror
    new JCommander.Builder().addObject(flags).build().parse(args);

    final String file = flags.input();
    SparkConf conf = new SparkConf().setAppName(flags.name);

    try (final JavaSparkContext ctx = new JavaSparkContext(conf)) {
      final JavaRDD<String> data = ctx.textFile(file).cache();
      final long numAs = countOccurances(data, flags.word);

      System.out.println("Lines with word: " + numAs );
    }
  }

  @SuppressWarnings("serial")
  private static long countOccurances(final JavaRDD<String> logData, final String str) {
    return logData.filter(new Function<String, Boolean>() {
      public Boolean call(final String s) throws Exception {
        return s.contains(str);
      }
    }).count();
  }
}
